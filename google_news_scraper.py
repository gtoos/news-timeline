from GoogleNews import GoogleNews
from newspaper import Article, Config
import newspaper
import pandas as pd
import nltk
import dateparser
import argparse

# parsing inputs
parser = argparse.ArgumentParser(description="Google News to Timeline JS.")
parser.add_argument('-s', action='store', dest='search_str', help='Search string')
parser.add_argument('-f', action='store', dest='from_date', help='From date mm/dd/yyyy format')
parser.add_argument('-t', action='store', dest='to_date', help='To date mm/dd/yyyy format')
parser.add_argument('-p', action='store', dest='pages', help='Number of pages to parse (x10 articles)')
parser.add_argument('-o', action='store', dest='output_path', help='output path (.xlsx)')

args = parser.parse_args()
search_str = args.search_str
from_date = args.from_date
to_date = args.to_date
pages = int(args.pages)
output_path = args.output_path

print("Generating timeline for '{}' between {} and {}".format(search_str, from_date, to_date))


nltk.download('punkt')

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
config = Config()
config.browser_user_agent = user_agent

gn = GoogleNews(start=from_date,end=to_date)
gn.search(search_str)
result = gn.result()

df = pd.DataFrame(result)
print(df.columns)

for i in range(2,pages+1):
    print("page", i)    
    gn.getpage(i)
    result=gn.result()
    df=pd.DataFrame(result)

timeline_js_cols = ["Year",	"Month", "Day",	"Time",	"End Year",	"End Month", "End Day",	"End Time",	"Display Date",	"Headline",	"Text",	"Media", "Media Credit", "Media Caption", "Media Thumbnail"]
ll=[]

for ind in df.index:
    dict={k: "" for k in timeline_js_cols}
    article = Article(df['link'][ind],config=config)
    print("downloading... {}".format(article.url))
    
    d = dateparser.parse(df['date'][ind])
    if d is not None:
        dict["Year"] = d.year
        dict["Month"] = d.month
        dict["Day"] = d.day
    
    dict['Display Date'] = df['date'][ind]
    dict['Media Credit']=df['media'][ind]
    dict['Media Caption']=df["link"][ind]

    try:
        article.download()
        article.parse()
        article.nlp()
        dict['Headline']=article.title
        dict['Text']=article.summary
        dict["Media"] = article.top_image

    except newspaper.article.ArticleException:
        print("skipping... ", df['link'][ind])
        dict['Text']=df['desc'][ind]
        dict["Media"] = df['img'][ind]
        dict['Headline'] = df['title'][ind]
        
    
    ll.append(dict)

news_df=pd.DataFrame(ll)
news_df.to_excel(output_path)
# news_df.to_excel("articles_{0}_{1}-{2}.xlsx".format("article", from_date, to_date))
