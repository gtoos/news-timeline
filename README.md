## Setting up on your machine
1. `git clone git@gitlab.com:gtoos/news-timeline.git`
2. `cd news-timeline`
3. `pip3 install -r requirements.txt`
4. Raise merge request for contributions!

* Using a virtual environment is recommended. 
* Tested with python3.7


## Running the script
```
Google News to Timeline JS.

optional arguments:
  -h, --help      show this help message and exit
  -s SEARCH_STR   Search string
  -f FROM_DATE    From date mm/dd/yyyy format
  -t TO_DATE      To date mm/dd/yyyy format
  -p PAGES        Number of pages to parse (x10 articles)
  -o OUTPUT_PATH  output path (.xlsx)
```

Example usage:
`python3 google_news_scraper.py -s '<your search string>' -f 09/01/2020 -t 09/31/2020 -p 20 -o articles_sep2020.xlsx`
